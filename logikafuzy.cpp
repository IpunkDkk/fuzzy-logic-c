#include <iostream>
#include <math.h>

using namespace std;

float detect_suhu(float suhu){
	int tamp_index;
	float tamp_dingin , tamp_sejuk, tamp_normal , tamp_hangat, tamp_panas , tamp_nilai=0;
	float tamp[4];

	//detect keanggotaan
	if (suhu < 15 && suhu >0){
		tamp_dingin = (15 - suhu) / (15 - 0);
		tamp_sejuk = (suhu - 10) / (15 - 10);
		tamp_normal = 0;
		tamp_hangat = 0;
		tamp_panas = 0;
	}else if (suhu == 0){
		tamp_dingin = 1;
		tamp_sejuk = 0;
		tamp_normal = 0;
		tamp_hangat = 0;
		tamp_panas = 0;
	}else if(suhu < 18 && suhu > 10){
		tamp_dingin = (15 - suhu)/(15 - 0);
		tamp_sejuk = (suhu - 10) / (18 - 10);
		tamp_normal = 0;
		tamp_hangat = 0;
		tamp_panas = 0;
	}else if(suhu > 18 && suhu <= 25){
		tamp_dingin = 0;
		tamp_sejuk = (25-suhu)/(25-18);
		tamp_normal = (suhu - 21)/(25-21);
		tamp_hangat = 0;
		tamp_panas = 0;
	}else if(suhu == 18){
		tamp_dingin = 0;
		tamp_sejuk = 1;
		tamp_normal = 0;
		tamp_hangat = 0;
		tamp_panas = 0;
	}else if (suhu < 25 && suhu >= 21){
		tamp_dingin = 0;
		tamp_sejuk = (25-suhu)/(25-18);
		tamp_normal = (suhu - 21)/( 25 - 21 );
		tamp_hangat = 0;
		tamp_panas = 0;
	}else if (suhu > 25 && suhu <=30){
		tamp_dingin = 0;
		tamp_sejuk = 0;
		tamp_normal = (30-suhu)/(30-25);
		tamp_hangat = (suhu-28)/(34-28);
		tamp_panas = 0;

	}else if(suhu == 25){
		tamp_dingin = 0;
		tamp_sejuk = 0;
		tamp_normal = 1;
		tamp_hangat = 0;
		tamp_panas = 0;
	}else if(suhu < 34 && suhu >= 28){
		tamp_dingin = 0;
		tamp_sejuk = 0;
		tamp_normal = (30-suhu) / (30-25);
		tamp_hangat = (suhu - 28)/(34-28);
		tamp_panas = 0;
	}else if(suhu > 34 && suhu <= 40){
		tamp_dingin = 0;
		tamp_sejuk = 0;
		tamp_normal = 0;
		tamp_hangat = (40 - suhu)/(40-34);
		tamp_panas = (suhu - 36)/(50-36);
	}else if(suhu == 34){
		tamp_dingin = 0;
		tamp_sejuk = 0;
		tamp_normal = 0;
		tamp_hangat = 1;
		tamp_panas = 0;
	}else if(suhu < 50 && suhu >= 36){
		tamp_dingin = 0;
		tamp_sejuk = 0;
		tamp_normal = 0;
		tamp_hangat = (40-suhu)/(40-34);
		tamp_panas = (suhu - 35)/(50-35);
	}else if(suhu == 50){
		tamp_dingin = 0;
		tamp_sejuk = 0;
		tamp_normal = 0;
		tamp_hangat = 0;
		tamp_panas = 1;
	}

	//mencari nilai maksimum dan menampung index

	tamp[0] = tamp_dingin;
	tamp[1] = tamp_sejuk;
	tamp[2] = tamp_normal;
	tamp[3] = tamp_hangat;
	tamp[4] = tamp_panas;

	for (int x = 0; x<=4; x++){
		if (tamp_nilai < tamp[x]){
			tamp_nilai = tamp[x];
			tamp_index = x;
		}
	}


	//mengatur nilai return agar lebih mudah
	if (tamp_index == 0){
		//dingin
		return 1;
	}else if(tamp_index == 1){
		//sejuk
		return 2;
	}else if (tamp_index == 2){
		//normal
		return 3;
	}else if(tamp_index == 3){
		//hangat
		return 4;
	}else if ( tamp_index == 4){
		//panas
		return 5;
	}else{
		//takterdifinis
		return 0;
	}
}

float detect_cahaya(float cahaya){
	float tamp_gelap, tamp_normal, tamp_terang;
	//detect anggota

	if(cahaya <= 35 && cahaya > 0){
		tamp_gelap = (35-cahaya)/(35-0);
		tamp_normal = (cahaya - 31)/(50-31);
		tamp_terang = 0;
	}else if(cahaya == 0){
		tamp_gelap  = 1;
		tamp_normal = 0;
		tamp_terang = 0;
	}else if(cahaya < 50 && cahaya >= 31){
		tamp_gelap = (35-suhu)/(35-0);
		tamp_normal = (suhu - 31)/(50-31);
		tamp_terang = 0;
	}else if(cahaya > 50 && cahaya <= 85){
		tamp_gelap  = 0;
		tamp_normal = (85-suhu)/(85-50);
		tamp_terang = (suhu - 81)/(100-81);
	}else if(cahaya == 50){
		tamp_gelap  = 0;
		tamp_normal = 1;
		tamp_terang = 0;
	}else if(cahaya<100 && cahaya >=81){
		tamp_gelap  = 0;
		tamp_normal = (85-suhu)/(85-50);
		tamp_terang = (suhu-81)/(100-81);
	}else if(cahaya==100){
		tamp_gelap  = 0;
		tamp_normal = 0;
		tamp_terang = 1;
	}

	if (cahaya >= 0 && cahaya <= 35){
		//gelap
		return 1;
	}else if(cahaya >= 31 && cahaya <= 85){
		//normal
		return 2;
	}else if(cahaya >= 81 && cahaya <= 100){
		//terang
		return 3;
	}else{
		return 0;
	}
}

float detect_mesin(float suhu , float cahaya){
	if (suhu == 1 && cahaya == 1){
		return 1;
	}else if (suhu == 1 && cahaya == 2){
		return 1;
	}else if (suhu == 1 && cahaya == 3){
		return 1;
	}else if (suhu == 2 && cahaya == 1){
		return 1;
	}else if (suhu == 2 && cahaya == 2){
		return 1;
	}else if (suhu == 2 && cahaya == 3){
		return 2;
	}else if (suhu == 3 && cahaya == 1){
		return 2;
	}else if (suhu == 3 && cahaya == 2){
		return 2;
	}else if (suhu == 3 && cahaya == 3){
		return 2;
	}else if (suhu == 4 && cahaya == 1){
		return 2;
	}else if (suhu == 4 && cahaya == 2){
		return 3;
	}else if (suhu == 4 && cahaya == 3){
		return 3;
	}else if (suhu == 5 && cahaya == 1){
		return 3;
	}else if (suhu == 5 && cahaya == 2){
		return 3;
	}else if (suhu == 5 && cahaya == 3){
		return 3;
	}else{
		return 0;
	}
}

void tampilsuhu(int suhu){
	if (suhu == 1){
		cout << "DIngin";
	}else if(suhu == 2){
		cout << "Sejuk";
	}else if (suhu == 3){
		cout << "Normal";
	}else if (suhu == 4){
		cout << "hangat";
	}else if (suhu == 5){
		cout << "Panas";
	}
}

void tampilcahaya(int cahaya){
	if (cahaya == 1){
		cout << "gelap";
	}else if (cahaya = 2){
		cout << "normal";
	}else if (cahaya == 3){
		cout << "terang";
	}

}

void tampilmesin(int mesin){
	if (mesin == 1){
		cout << "lambat";
	}else if(mesin == 2){
		cout << "sedang";
	}else if(mesin == 3){
		cout << "cepat";
	}


}


int main(int argc, char const *argv[])
{

	
	tampilsuhu(detect_suhu(10));
	return 0;
}
